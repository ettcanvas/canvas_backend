package com.ar.canvas.model;

import java.io.Serializable;

public class GraphModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private String graphType;
    private String value;
    private String parameter;
    private String color;

    public GraphModel ( String graphType, String value, String parameter, String color ) {
        this.graphType = graphType;
        this.value = value;
        this.parameter = parameter;
        this.color = color;
    }
    public GraphModel(){}
    public String getGraphType ( ) {
        return graphType;
    }

    public void setGraphType ( String graphType ) {
        this.graphType = graphType;
    }

    public String getValue ( ) {
        return value;
    }

    public void setValue ( String value ) {
        this.value = value;
    }

    public String getParameter ( ) {
        return parameter;
    }

    public void setParameter ( String parameter ) {
        this.parameter = parameter;
    }

    public String getColor ( ) {
        return color;
    }

    public void setColor ( String color ) {
        this.color = color;
    }

    @Override
    public String toString ( ) {
        return "GraphModel{" +
            "graphType='" + graphType + '\'' +
            ", value='" + value + '\'' +
            ", parameter='" + parameter + '\'' +
            ", color='" + color + '\'' +
            '}';
    }
}
