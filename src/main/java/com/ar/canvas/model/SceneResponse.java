package com.ar.canvas.model;

public class SceneResponse {
    
    private  String SceneName;
    private String SceneHash;
    private String contentType;
    private Scene scene;

    public SceneResponse ( String sceneName, String sceneHash, String contentType, Scene scene ) {
        SceneName = sceneName;
        SceneHash = sceneHash;
        this.contentType = contentType;
        this.scene = scene;
    }

    public SceneResponse (){}
    public String getSceneName ( ) {
        return SceneName;
    }

    public void setSceneName ( String sceneName ) {
        SceneName = sceneName;
    }

    public String getSceneHash ( ) {
        return SceneHash;
    }

    public void setSceneHash ( String sceneHash ) {
        SceneHash = sceneHash;
    }

    public String getContentType ( ) {
        return contentType;
    }

    public void setContentType ( String contentType ) {
        this.contentType = contentType;
    }

    public Scene getScene ( ) {
        return scene;
    }

    public void setScene ( Scene scene ) {
        this.scene = scene;
    }

    @Override
    public String toString ( ) {
        return "SceneResponse{" +
            "SceneName='" + SceneName + '\'' +
            ", SceneHash='" + SceneHash + '\'' +
            ", contentType='" + contentType + '\'' +
            ", scene=" + scene +
            '}';
    }
}
