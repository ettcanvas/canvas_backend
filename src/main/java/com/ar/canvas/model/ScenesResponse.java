package com.ar.canvas.model;

import java.util.Map;

public class ScenesResponse {
    private  String SceneName;
    private String SceneHash;
    private String contentType;
    private Map<String,Scene> sceneMap;


    public ScenesResponse ( String sceneName, String sceneHash, String contentType, Map <String, Scene> sceneMap ) {
        SceneName = sceneName;
        SceneHash = sceneHash;
        this.contentType = contentType;
        this.sceneMap = sceneMap;
    }
    public ScenesResponse (){}


    public String getSceneName ( ) {
        return SceneName;
    }

    public void setSceneName ( String sceneName ) {
        SceneName = sceneName;
    }

    public String getSceneHash ( ) {
        return SceneHash;
    }

    public void setSceneHash ( String sceneHash ) {
        SceneHash = sceneHash;
    }

    public String getContentType ( ) {
        return contentType;
    }

    public void setContentType ( String contentType ) {
        this.contentType = contentType;
    }

    public Map <String, Scene> getSceneMap ( ) {
        return sceneMap;
    }

    public void setSceneMap ( Map <String, Scene> sceneMap ) {
        this.sceneMap = sceneMap;
    }

    @Override
    public String toString ( ) {
        return "ScenesResponse{" +
            "SceneName='" + SceneName + '\'' +
            ", SceneHash='" + SceneHash + '\'' +
            ", contentType='" + contentType + '\'' +
            ", sceneMap=" + sceneMap +
            '}';
    }
}
