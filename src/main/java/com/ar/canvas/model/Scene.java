
package com.ar.canvas.model;


import com.ar.canvas.model.enumeration.SceneType;
import com.ar.canvas.model.enumeration.ScreenType;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class Scene implements Serializable {
    // corporate informations
    private static final long serialVersionUID = 1L;

    private String  sequenceNo;
     private SceneType sceneType;
     private String SpeechData ;
     private List<Scene>  scene;
     private ScreenType screenType;
     private List<GraphModel> graphModels;

    public Scene ( String sequenceNo, SceneType sceneType, String speechData, List <Scene> scene, ScreenType screenType, List<GraphModel> graphModels ) {
        this.sequenceNo = sequenceNo;
        this.sceneType = sceneType;
        SpeechData = speechData;
        this.scene = scene;
        this.screenType = screenType;
        this.graphModels = graphModels;
    }
    public Scene (){}


    public SceneType getSceneType ( ) {
        return sceneType;
    }

    public void setSceneType ( SceneType sceneType ) {
        this.sceneType = sceneType;
    }

    public String getSpeechData ( ) {
        return SpeechData;
    }

    public void setSpeechData ( String speechData ) {
        SpeechData = speechData;
    }

    public List <Scene> getScene ( ) {
        return scene;
    }

    public void setScene ( List <Scene> scene ) {
        this.scene = scene;
    }

    public ScreenType getScreenType ( ) {
        return screenType;
    }

    public void setScreenType ( ScreenType screenType ) {
        this.screenType = screenType;
    }

    public String getSequenceNo ( ) {
        return sequenceNo;
    }

    public void setSequenceNo ( String sequenceNo ) {
        this.sequenceNo = sequenceNo;
    }

    public List <GraphModel> getGraphModels ( ) {
        return graphModels;
    }

    public void setGraphModels ( List <GraphModel> graphModels ) {
        this.graphModels = graphModels;
    }

    @Override
    public String toString ( ) {
        return "Scene{" +
            "sequenceNo='" + sequenceNo + '\'' +
            ", sceneType=" + sceneType +
            ", SpeechData='" + SpeechData + '\'' +
            ", scene=" + scene +
            ", screenType=" + screenType +
            ", graphModels=" + graphModels +
            '}';
    }
}
