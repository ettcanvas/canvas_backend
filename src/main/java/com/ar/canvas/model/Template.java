package com.ar.canvas.model;

public class Template {
    private  String templateName;
    private String templateHash;
    private String contentType;
    private byte[] fileByte;

    public Template(String templateName, String templateHash, String contentType, byte[] fileByte) {
        this.templateName = templateName;
        this.templateHash = templateHash;
        this.contentType = contentType;
        this.fileByte = fileByte;
    }

    public Template(){

    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateHash() {
        return templateHash;
    }

    public void setTemplateHash(String templateHash) {
        this.templateHash = templateHash;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getFileByte() {
        return fileByte;
    }

    public void setFileByte(byte[] fileByte) {
        this.fileByte = fileByte;
    }

    @Override
    public String toString() {
        return "templateDetails{" +
            "templateName='" + templateName + '\'' +
            ", templateHash='" + templateHash + '\'' +
            ", contentType='" + contentType + '\'' +
            '}';
    }
}
