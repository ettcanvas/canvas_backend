package com.ar.canvas.amazon;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.ar.canvas.config.AwsS3Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

@Component
public class ObjectStorageService {

    //  private  static final AWSCredentials credentials;
    private static String bucketName;

    @Autowired
    private AwsS3Configuration awsS3Configuration;

    private final Logger log = LoggerFactory.getLogger(ObjectStorageService.class);
    private static AmazonS3 s3client;

    public void putDocumentS3(MultipartFile file) throws IOException {
        bucketName = awsS3Configuration.getBucketName();
        // getting hash of object
        String key = generateHash(file);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.addUserMetadata("filename",file.getOriginalFilename());
        objectMetadata.setContentType(file.getContentType());
        objectMetadata.setContentLength(file.getSize());
        // uploading object
        awsS3Configuration.getAwsService().putObject(bucketName,key,file.getInputStream(),objectMetadata);
    }

    public String putDocumentBytesS3(byte[] byteData,String filename, String contentType) throws IOException {
        bucketName = awsS3Configuration.getBucketName();
        // getting hash of object
        String key = generateHash(byteData);
        log.info ( "key generated for file:"+key );
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.addUserMetadata("filename",filename);
        objectMetadata.setContentType(contentType);
        objectMetadata.setContentLength(byteData.length);
        // uploading object
        ByteArrayInputStream bis = new ByteArrayInputStream(byteData);
        awsS3Configuration.getAwsService().putObject(bucketName,key,bis,objectMetadata);
        return key;
    }

    public Map<String,String> getDocumentNames(){
        bucketName = awsS3Configuration.getBucketName();
        ObjectListing objectListing =  awsS3Configuration.getAwsService().listObjects(bucketName);
        Map<String,String> fileMap = new HashMap<>();
        for (S3ObjectSummary os : objectListing.getObjectSummaries()) {
            log.debug("save os.getKey( : {}", os.getKey());
            System.out.println(os.getKey());
            ObjectMetadata objectMetadata =  awsS3Configuration.getAwsService().getS3client().getObjectMetadata(bucketName, os.getKey());
            Map userMetadataMap = objectMetadata.getUserMetadata();
            fileMap.put(os.getKey(),userMetadataMap.get("filename").toString());
        }
        return fileMap;
    }

    public S3Object getDocument(String key){
        bucketName = awsS3Configuration.getBucketName();
        S3Object s3Object =  awsS3Configuration.getAwsService().getObject(bucketName,key);
        return s3Object;
    }

    public boolean checkObjectPresent(String key){
        bucketName = awsS3Configuration.getBucketName();
        return awsS3Configuration.getAwsService().getS3client().doesObjectExist(bucketName,key);
    }

    public String generateHash(MultipartFile file) {
        byte[] buffer = new byte[8192];
        int count;
        BufferedInputStream bis = null;
        byte[] hashInBytes = null;
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            bis = new BufferedInputStream(file.getInputStream());
            while ((count = bis.read(buffer)) > 0) {
                digest.update(buffer, 0, count);
            }
            bis.close();

            hashInBytes = digest.digest();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) {
                    bis.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return sb.toString();
    }

    public String generateHash(byte[] byteData) {
        byte[] hashInBytes = null;
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(byteData, 0, byteData.length);
            hashInBytes = digest.digest();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            System.out.println(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return sb.toString();
    }


//    static {
//        //put your accesskey and secretkey here
//      /*  credentials = new BasicAWSCredentials(
//            "AKIAJSFO3V7JKK6A43HA",
//            "dP64QEUbpx62TGkr1vCpRiarvrSnf6mxX98dAuIN"
//        )*/
//        ;
//
//
//       /* s3client= AmazonS3ClientBuilder
//            .standard().withRegion(Regions.US_EAST_1).build();
//
//      */   /*s3client = AmazonS3ClientBuilder
//            .standard()
//            .withCredentials(new AWSStaticCredentialsProvider(credentials))
//            .withRegion(Regions.US_EAST_1) // N.virginia // https://docs.aws.amazon.com/general/latest/gr/rande.html
//            .build();*/
//
//    }
    //list all the buckets
//        for (Bucket s : awsService.listBuckets()) {
//            System.out.println(s.getName());
//        }

    //listing objects
//        ObjectListing objectListing = awsService.listObjects(bucketName);
//        for (S3ObjectSummary os : objectListing.getObjectSummaries()) {
//            log.debug("save os.getKey( : {}", os.getKey());
//            System.out.println(os.getKey());
//        }

    //downloading an object
//       S3Object s3object = awsService.getObject(bucketName, key);
//       System.out.println("Printing metadata");
//       System.out.println(s3object.getObjectMetadata().getUserMetadata().get("filename"));
//        S3ObjectInputStream inputStream = s3object.getObjectContent();
//       // FileUtils.copyInputStreamToFile(inputStream, new File("/Users/user/Desktop/hello.txt"));
//        Files.copy(inputStream,"/Users/user/Desktop/hello.txt", StandardCopyOption.REPLACE_EXISTING);

}
