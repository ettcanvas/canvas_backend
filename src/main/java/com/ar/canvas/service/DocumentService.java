package com.ar.canvas.service;

import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.ar.canvas.amazon.ObjectStorageService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;


@Service
public class DocumentService {

    private final Logger log = LoggerFactory.getLogger ( DocumentService.class );

    @Autowired
    private ObjectStorageService objectStorageService;

    public String uploadFile ( MultipartFile file ) throws IOException {
        objectStorageService.putDocumentS3 ( file );
        return "File Uploaded";
    }

    public Map <String, String> getDocumentNames ( ) {
        return objectStorageService.getDocumentNames ( );
    }

    public boolean checkKeyExistObjectStorage ( MultipartFile file ) {
        try {
            log.info ( "objectStorageService.generateHash " );
            getFile ( objectStorageService.generateHash ( file ) );
            return true;
        } catch (Exception e) {
            log.error ( "objectStorageService.generateHash "+e );
            return false;

        }

    }

    public S3Object getFile ( String key ) {
        S3Object s3Object = objectStorageService.getDocument ( key );
        return s3Object;
    }
}
