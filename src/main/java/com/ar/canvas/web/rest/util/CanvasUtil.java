package com.ar.canvas.web.rest.util;

import com.ar.canvas.service.DocumentService;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;


public class CanvasUtil {
    private final Logger log = LoggerFactory.getLogger ( CanvasConverter.class );

    public static boolean checkFileExtensionForGlb ( MultipartFile file ) {
        String ext = FilenameUtils.getExtension ( file.getOriginalFilename ( ) );
        if (!ext.equalsIgnoreCase ( "glb" )) {
            return false;
        }
        return true;
    }

    public static byte[] serialize ( Object obj ) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream ( );
        ObjectOutputStream os = new ObjectOutputStream ( out );
        os.writeObject ( obj );
        return out.toByteArray ( );
    }

    public static Object deserialize ( byte[] data ) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream ( data );
        ObjectInputStream is = new ObjectInputStream ( in );
        return is.readObject ( );
    }

    public static boolean checkFileExtensionForObject ( MultipartFile file ) {
        String ext = FilenameUtils.getExtension ( file.getOriginalFilename ( ) );
        if (!ext.equalsIgnoreCase ( "obj" )) {
            return false;
        }
        return true;
    }

    public static void deleteFilesFromDir ( String fileName ) {
        File dir = new File ( fileName );
        if (dir.isDirectory ( ) == false) {
            System.out.println ( "Not a directory. Do nothing" );
            return;
        }
        File[] listFiles = dir.listFiles ( );
        for (File file : listFiles) {
            System.out.println ( "Deleting " + file.getName ( ) );
            file.delete ( );
        }

    }


}
