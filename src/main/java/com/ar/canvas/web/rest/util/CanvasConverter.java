package com.ar.canvas.web.rest.util;

import org.apache.commons.io.FileUtils;
import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class CanvasConverter {
    private final Logger log = LoggerFactory.getLogger ( CanvasConverter.class );

    public void convertFBXToSFB () {
        GradleConnector connector;
        connector = GradleConnector.newConnector ( );
        connector.useInstallation ( new File ( "G:\\gradle\\gradle-5.2.1-bin\\gradle-5.2.1" ) );
        connector.forProjectDirectory ( new File ( "G:\\gltf-import-export\\gltmp\\canvas_backend" ) );
        ProjectConnection connection = connector.connect ( );
        BuildLauncher build = connection.newBuild ( );
        log.info ( "create tasks createAsset and compileAsset creating sfa and sfb files" );
        build.forTasks ( "createAsset" );
        build.forTasks ( "compileAsset" );
        log.info ( "start run sfb task" );
        build.run ( );
        log.info ( "succesfully convert object to sfb!" );
        connection.close ( );
    }

    public void generateGLBToGlTf(String fileName) throws  IOException{
        String line = executeCommanLine ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin", "assimp.exe export " + fileName + ".glb gltf\\" + fileName + "\\" + fileName + ".gltf" );
        log.info ( "succesfully convert glb to gltf!" + line );
        String extractTextLine = executeCommanLine ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin", " assimp.exe extract  gltf\\" + fileName + "\\" + fileName + ".gltf" );
        log.info ( "extract texture from image" + extractTextLine );

    }

    public void convertGLBToGlTF ( String fileName ) throws IOException {
        String fileNames = "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\gltf\\"+fileName;
        Path path = Paths.get (fileNames );
        log.info ("fileNames"+fileNames );
        if (!Files.exists ( path )) {

            Files.createDirectory (path);
            log.info ( "Directory created" );
            generateGLBToGlTf ( fileName );
            log.info ( "generateGLBToGLTF with new dir()" );
        }
        File fs = new File ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\gltf\\" + fileName + "\\" + fileName + ".gltf" );
        if (!fs.exists ( )) {
            generateGLBToGlTf ( fileName );
            log.info ( "generateGLBToGLTF" );
        }


    }

    private static List <File> listOfFileToBeCopied ( String extension, String dire ) throws IOException {
        File dir = new File ( dire );
        String[] extensions = new String[]{extension};
        System.out.println ( "Getting all ..PNG files in " + dir.getCanonicalPath ( )
            + " including those in subdirectories" );
        List <File> files = (List <File>) FileUtils.listFiles ( dir, extensions, true );
        for (File file : files) {
            System.out.println ( "file: " + file.getCanonicalPath ( ) );
        }
        return files;
    }

    public void copyFileUsingJava7Files ( File source, File dest ) throws IOException {
        Files.copy ( source.toPath ( ), dest.toPath ( ) );
    }



    public void generateGLBToFBX ( String fileName ) throws IOException {
        convertGLBToGlTF ( fileName );
        log.info ( "convertGLBToFBX()" );
        List <File> files = listOfFileToBeCopied ( "png", "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\gltf\\" + fileName + "\\" );
        log.info ( "listOfFileToBeCopied()" );
        for (File file : files) {
            File dest = new File ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\fbx\\" + fileName + "\\" + file.getName ( ) );
            copyFileUsingJava7Files( file, dest );
            log.info ( "copyFileUsingJava7Files()" );
        }
        String line = executeCommanLine ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin", " assimp.exe export " + fileName + ".glb fbx\\" + fileName + "\\" + fileName + ".fbx" );
        log.info ( "succesfully convert glb to fbx!" + line );
    }

    public void convertGLBToFBX ( String fileName ) throws IOException {
        String fileNames = "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\fbx\\" + fileName;
        Path path = Paths.get ( fileNames );
        if (!Files.exists ( path )) {
            Files.createDirectory ( path );
            log.info ( "Directory created" );
            generateGLBToFBX ( fileName );
            log.info ( "generateGLBToFBX with new dir()" );
        } else {
            File fs = new File ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\fbx\\" + fileName + "\\" + fileName + ".fbx" );
            if (!fs.exists ( )) {

                generateGLBToFBX ( fileName );
                log.info ( "generateGLBToFBX()" );
            }
        }
    }


    public void generateGLBToObject ( String fileName ) throws IOException {
        convertGLBToGlTF ( fileName );
        log.info ( "convertGLBToObj()" );
        List <File> files = listOfFileToBeCopied ( "png", "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\gltf\\" + fileName + "\\" );
        log.info ( "listOfFileToBeCopied()" );
        for (File file : files) {
            File dest = new File ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\object\\" + fileName + "\\" + file.getName ( ) );
            copyFileUsingJava7Files ( file, dest );
            log.info ( "copyFileUsingJava7Files()" );


        }

        String line = executeCommanLine ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin", " assimp.exe export " + fileName + ".glb object\\" + fileName + "\\" + fileName + ".obj" );
        log.info ( "succesfully convert glb to object!" + line );

    }


    public void convertGLBToObject ( String fileName ) throws IOException {
        String fileNames = "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\object\\" + fileName;

        Path path = Paths.get ( fileNames );
        if (!Files.exists ( path )) {

            Files.createDirectory ( path );
            log.info ( "Directory created" );
            generateGLBToObject ( fileName );
            log.info ( "generateGLBToObject with new dir()" );

        } else {
            File fs = new File ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\object\\" + fileName + "\\" + fileName + ".obj" );
            if (!fs.exists ( )) {
                generateGLBToObject ( fileName );
                log.info ( "convertGLBToObject()" );
            }
        }

    }


    public String executeCommanLine ( String filePath, String cmd ) throws IOException {
        ProcessBuilder builder = new ProcessBuilder (
            "cmd.exe", "/c, cd  " + filePath + " && " + cmd );
        builder.redirectErrorStream ( true );
        Process p = builder.start ( );
        BufferedReader r = new BufferedReader ( new InputStreamReader ( p.getInputStream ( ) ) );
        String line;
        while (true) {
            line = r.readLine ( );
            if (line == null) {
                break;
            }
        }
        log.info ( "succesfully executeCommanLine()" + line );
        return line;
    }

    @Override
    public String toString ( ) {
        return super.toString ( );
    }
}
