/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ar.canvas.web.rest.vm;
