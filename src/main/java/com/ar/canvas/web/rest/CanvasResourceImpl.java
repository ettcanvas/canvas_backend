package com.ar.canvas.web.rest;

import com.amazonaws.services.s3.model.S3Object;
import com.ar.canvas.amazon.ObjectStorageService;
import com.ar.canvas.model.Scene;
import com.ar.canvas.model.SceneResponse;
import com.ar.canvas.model.ScenesResponse;
import com.ar.canvas.model.Template;
import com.ar.canvas.model.enumeration.ContentType;
import com.ar.canvas.service.DocumentService;
import com.ar.canvas.web.rest.util.CanvasConverter;
import com.ar.canvas.web.rest.util.CanvasUtil;
import com.codahale.metrics.annotation.Timed;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class CanvasResourceImpl {


    private final Logger log = LoggerFactory.getLogger ( CanvasResourceImpl.class );


    @Autowired
    CanvasConverter canvasConverter;
    @Autowired
    private ObjectStorageService objectStorageService;

    @Autowired
    private DocumentService documentService;

    @PostMapping("/uploadGLBImage")
    public String uploadGLBFileImage ( @RequestParam("file") MultipartFile file ) throws IOException {
        log.info ( "upload Multipart file()" + file.getOriginalFilename ( ) );
        boolean extn = CanvasUtil.checkFileExtensionForGlb ( file );
        if (!extn) {
            return "invalid extension!!";
        }
        if (documentService.checkKeyExistObjectStorage ( file )) {
            return "file already exist!!";
        }
        return documentService.uploadFile ( file );

    }
    @PostMapping("/uploadTemplate")
    public String uploadGLBFileImage ( Template template ) throws IOException {
        log.info ( "upload template file()" + template.getTemplateName ( ) );
        return objectStorageService.putDocumentBytesS3 ( template.getFileByte (), template.getTemplateName ( ), ContentType.TEMPLATE.toString ());
    }

    @PostMapping("/storeScene")
    public String storeScene ( Scene scene ) throws IOException {
        byte[] sceneByteArray = CanvasUtil.serialize ( scene );
        log.info ( "storeScene() to s3 storage" );
        return objectStorageService.putDocumentBytesS3 ( sceneByteArray, scene.getSceneType ( ).toString ( ), ContentType.SCENEOBJECT.toString () );
    }

    @PostMapping("/storeScenes")
    public String storeScenes ( Map <String, Scene> multipleScene ) throws IOException {
        if (multipleScene != null && multipleScene.size ( ) > 0) {
            byte[] sceneByteArray = CanvasUtil.serialize ( multipleScene );
            log.info ( "storeMultipleScene() to s3 storage" );
            return objectStorageService.putDocumentBytesS3 ( sceneByteArray, multipleScene.get ( 0 ).getSceneType ( ).toString ( ), ContentType.SCENEOBJECT.toString ());
        } else {
            log.info ( "storeMultipleScene() No data Exist!!" );
            return "No data Exist!!";

        }

    }

    @PostMapping("/uploadObjectImage")
    public String uploadObjectFileImage ( @RequestParam("file") MultipartFile file ) throws IOException {
        log.info ( "uploadObjectFileImage Multipart file()" + file.getOriginalFilename ( ) );
        boolean extn = CanvasUtil.checkFileExtensionForObject ( file );
        if (!extn) {
            return "invalid extension!!";
        }
        if (documentService.checkKeyExistObjectStorage ( file )) {
            return "file already exist!!";
        }
        return documentService.uploadFile ( file );
    }

    private static List <File> listOfFileToBeCopied ( String extension, String dire ) throws IOException {
        File dir = new File ( dire );
        String[] extensions = new String[]{extension};
        System.out.println ( "Getting all ..PNG files in " + dir.getCanonicalPath ( )
            + " including those in subdirectories" );
        List <File> files = (List <File>) FileUtils.listFiles ( dir, extensions, true );
        for (File file : files) {
            System.out.println ( "file: " + file.getCanonicalPath ( ) );
        }
        return files;
    }

    @PostMapping("/convertObjectImageFromGLB")
    public String uploadObjectImageFromGLB ( @RequestParam("file") MultipartFile file ) throws IOException {
        log.info ( "uploadFileImageForConverter()" + file.getOriginalFilename ( ) );
        boolean extn = CanvasUtil.checkFileExtensionForGlb ( file );
        if (!extn) {
            log.info ( "uploadGLBImageAndConvert()" + "invalid extension" );
            return "invalid extension!!";
        }
        // String key = objectStorageService.generateHash ( file );
        String key = file.getOriginalFilename ( );
        key = key.split ( "." )[0];
        byte[] b = file.getBytes ( );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".glb" ), b );
        canvasConverter.convertGLBToObject ( key );
        log.info ( "glb to  object" );
        return "converted succesfully!";
    }


    @PostMapping("/uploadSfbImageFromGLB")
    public String uploadSfbImageFromGLB ( @RequestParam("file") MultipartFile file ) throws IOException {
        log.info ( "uploadFileImageForConverter()" + file.getOriginalFilename ( ) );
        boolean extn = CanvasUtil.checkFileExtensionForGlb ( file );
        if (!extn) {
            log.info ( "uploadGLBImageAndConvert()" + "invalid extension" );
            return "invalid extension!!";
        }
        // String key = objectStorageService.generateHash ( file );
        String key = file.getOriginalFilename ( );
        log.info ( "key is " + key );

        key = key.split ( "\\." )[0];
        byte[] b = file.getBytes ( );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".glb" ), b );
        canvasConverter.convertGLBToFBX ( key );
        log.info ( "glb to fbx object" );
        CanvasUtil.deleteFilesFromDir ( "G:\\gltf-import-export\\gltmp\\canvas_backend\\sampledata\\models" );
        List <File> files = listOfFileToBeCopied ( "png", "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\fbx\\" + key + "\\" );
        log.info ( "listOfFileToBeCopied()" );
        for (File filey : files) {
            File dest = new File ( "G:\\gltf-import-export\\gltmp\\canvas_backend\\sampledata\\models\\" + filey.getName ( ) );
            canvasConverter.copyFileUsingJava7Files ( filey, dest );
            log.info ( "copyFileUsingJava7Files()" );
        }

        canvasConverter.copyFileUsingJava7Files ( new File ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\fbx\\" + key + "\\" + key + ".fbx" ), new File ( "G:\\gltf-import-export\\gltmp\\canvas_backend\\sampledata\\models\\fbx.fbx" ) );
        canvasConverter.convertFBXToSFB ( );
        byte[] byteArray = null;
        String path = "G:\\gltf-import-export\\gltmp\\canvas_backend\\sampledata\\models\\fbx.sfb";
        byte[] encoded = Files.readAllBytes ( Paths.get ( path ) );
        System.out.println ( Arrays.toString ( encoded ) );
        objectStorageService.putDocumentBytesS3 ( encoded, "fbx.sfb", file.getName ( ) );
        return "converted succesfully!";
    }


    @GetMapping("/getSfbObject")
    @Timed
    public Template getSfbObjectFromS3 ( @RequestParam("key") String key ) throws IOException {
        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        log.info ( "getTemplate() from s3 storage" );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".sfb" ), byteArray );

        return new Template (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            byteArray);
          }


    @GetMapping("/getFBXObject")
    @Timed
    public Template getFBXObjectFromS3 ( @RequestParam("key") String key ) throws IOException {
        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".fbx" ), byteArray );
        log.info ( "getFBXObjectFromS3() from s3 storage" );

        return new Template (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            byteArray);


    }

    @GetMapping("/getGLTFObject")
    @Timed
    public Template getGLTFObjectFromS3 ( @RequestParam("key") String key ) throws IOException {

        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".gltf" ), byteArray );

        log.info ( "getGLTFObjectFromS3() from s3 storage" );
        return new Template (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            byteArray);


    }

    @GetMapping("/getObject")
    @Timed
    public Template getObjectFromS3 ( @RequestParam("key") String key ) throws IOException {

        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".obj" ), byteArray);
        log.info ( "getObjectFromS3() from s3 storage" );
        return new Template (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            byteArray);


    }


    @GetMapping("/getGLBFile")
    @Timed
    public Template getGLBFile ( @RequestParam("key") String key ) throws IOException {
        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        Files.write ( Paths.get ( "G:\\gltf-import-export\\gltmp\\assimp-20190318T071851Z-001\\assimp\\bin\\" + key + ".glb" ), byteArray );
        log.info ( "getGLBFile() from s3 storage" );
        return new Template (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            byteArray);


    }
    @GetMapping("/getTemplate")
    @Timed
    public Template getTemplate ( @RequestParam("key") String key ) throws IOException {

        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        log.info ( "getTemplate() from s3 storage" );
        return new Template (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            byteArray);

    }
    @GetMapping("/getScene")
    @Timed
    public SceneResponse getScene ( @RequestParam("key") String key ) throws IOException, ClassNotFoundException {

        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        log.info ( "getScene() from s3 storage" );
        Scene scene= (Scene) CanvasUtil.deserialize (byteArray );
        log.info ( "getScene() deserilize" );
        return new SceneResponse (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            scene);

    }

    @GetMapping("/getScenes")
    @Timed
    public ScenesResponse getScenes ( @RequestParam("key") String key ) throws IOException, ClassNotFoundException {
        S3Object s3Object = documentService.getFile ( key ); // Initialize this to the File path you want to serve.
        InputStreamResource resource = new InputStreamResource ( s3Object.getObjectContent ( ) );
        byte[] byteArray = IOUtils.toByteArray ( resource.getInputStream ( ) );
        log.info ( "getScene() from s3 storage" );
        Map<String,Scene> sceneMap= (Map <String, Scene>) CanvasUtil.deserialize (byteArray );
        log.info ( "getScene() deserilize" );
        return new ScenesResponse (s3Object.getObjectMetadata().getUserMetadata().get("filename"),
            key,
            s3Object.getObjectMetadata().getContentType(),
            sceneMap);

    }

}
