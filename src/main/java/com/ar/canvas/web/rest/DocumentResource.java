package com.ar.canvas.web.rest;


import com.amazonaws.services.s3.model.S3Object;
import com.ar.canvas.service.DocumentService;
import com.ar.canvas.web.rest.UserResource;
import com.codahale.metrics.annotation.Timed;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class DocumentResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private final DocumentService documentService;
    public DocumentResource ( DocumentService documentService){
        this.documentService = documentService;
    }

    @PostMapping("/upload")
    public String uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        System.out.println(file.getOriginalFilename());
        return documentService.uploadFile(file);
    }

    @GetMapping("/documents")
    @Timed
    public Map<String,String> getFileNames() {
        return  documentService.getDocumentNames();
    }

    @GetMapping("/documents/document")
    public byte[] getFile(@RequestParam("key") String key) throws IOException {
        S3Object s3Object = documentService.getFile(key); // Initialize this to the File path you want to serve.

        InputStreamResource resource = new InputStreamResource(s3Object.getObjectContent());
        byte[] b = IOUtils.toByteArray(resource.getInputStream());
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+s3Object.getObjectMetadata().getUserMetadata().get("filename"));
        headers.add(HttpHeaders.CONTENT_TYPE, s3Object.getObjectMetadata().getContentType());
        return b;

        //        return ResponseEntity.ok()
//            .headers(headers)
//            .contentLength(s3Object.getObjectMetadata().getContentLength())
//            .contentType(MediaType.valueOf(s3Object.getObjectMetadata().getContentType()))
//            .body(resource);
    }

}
