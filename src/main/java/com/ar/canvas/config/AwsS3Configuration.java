package com.ar.canvas.config;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.ar.canvas.amazon.AWSS3Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsS3Configuration {

    @Value("${aws.bucketName}")
    private String bucketName;

    @Value("${aws.accessKey:#{null}}")
    private String accessKey;

    @Value("${aws.secretKey:#{null}}")
    private String secretKey;

    @Autowired
    AWSS3Service awsService;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    private final Logger log = LoggerFactory.getLogger(AwsS3Configuration.class);

    @Bean
    public AWSS3Service getAwsService() {
        AWSCredentials credentials = null;
        try {
            if(getAccessKey() != null){
                // if access key is present in envirnment
                log.info("Getting credentials from environment");
                System.out.println (getAccessKey() );
                log.info("Getting credentials from getAccessKey machine"+getAccessKey ()+getSecretKey ());
                System.out.println (getSecretKey () );
                credentials = new BasicAWSCredentials(getAccessKey(),getSecretKey());
            }
            else{
                //get credentials from local machine ~/.aws/credentials
                // https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html
                log.info("Getting credentials from local machine");
                credentials = new ProfileCredentialsProvider().getCredentials();
            }

            log.debug("ss  credentials  "+credentials);
        } catch (Exception e) {
            throw new AmazonClientException(
                "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                e);
        }
     /*   AmazonS3 s3 = AmazonS3ClientBuilder.standard()
            .withCredentials( DefaultAWSCredentialsProviderChain.getInstance())
            .build();*/
        AmazonS3 s3 = new AmazonS3Client ();
        Region usWest2 = Region.getRegion(Regions.EU_WEST_3);
        s3.setRegion(usWest2);
        awsService.setS3client ( s3 );

       /* AmazonS3 s3 = AmazonS3ClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .withRegion( Regions.US_EAST_1)
            .build();
        awsService.setS3client(s3);*/
        return awsService;
    }
}
